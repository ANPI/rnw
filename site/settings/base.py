import os
import sys
import environ
root = environ.Path(__file__) - 2 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), ) # set default values and casting
environ.Env.read_env() # reading .env file

SITE_ROOT = root()

DEBUG = env('DEBUG') # False if not in os.environ
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': env.db(), # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
#    'extra': env.db('SQLITE_URL', default='sqlite:////tmp/my-tmp-sqlite.db')
}

public_root = root.path('staticfiles/')

EMAIL_HOST = env('DJANGO_EMAIL_HOST', default='localhost')
MEDIA_ROOT = '/data/rnwfiles'
MEDIA_URL = env('DJANGO_MEDIA_URL', default='/media/')
STATIC_ROOT = public_root()
STATIC_URL = env('DJANGO_STATIC_URL', default='/static/')

RNW_API = env("RNW_API", default='/rnw/api')

USE_X_FORWARDED_HOST=True

SECRET_KEY = env('DJANGO_SECRET_KEY') # Raises ImproperlyConfigured exception if SECRET_KEY not in os.environ

CACHES = {
    'default': env.cache(),
    #'redis': env.cache('REDIS_URL')
}
IN_TESTING = sys.argv[1:2] == ['test']

ALLOWED_HOSTS = env('ALLOWED_HOSTS', default='locahost')

# Application definition

INSTALLED_APPS = [
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework_swagger',
]

PROJECT_APPS = ['rnw']

INSTALLED_APPS += PROJECT_APPS

REST_FRAMEWORK = {
#    'DEFAULT_PERMISSION_CLASSES': { 'rest_framework.permissions.IsAdminUser' ,},
    'VIEW_DESCRIPTION_FUNCTION': 'rest_framework_swagger.views.get_restructuredtext',
    'PAGE_SIZE': 20
}

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = env('ROOT_URLCONF', default='urls')

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application'


# Internationalization

LANGUAGE_CODE = 'en-gb'

TIME_ZONE = 'UTC'

USE_I18N = env.bool('USE_I18N', default=False)

USE_L10N = True

USE_TZ = True



# Additional locations of static files

STATICFILES_DIRS = (
      env.list('DJANGO_STATICFILES_DIRS', default=[])
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [
            root('templates'),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    }
]

# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# .local.py overrides all the common settings.
try:
    from .local import *  # noqa
except ImportError:
    pass


# importing test settings file if necessary
if IN_TESTING:
    from .testing import *  # noqa
