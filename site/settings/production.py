from .base import *  # noqa
import os


DEBUG = False
TEMPLATES['OPTIONS']['debug'] = DEBUG

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', 'localhost').split(',')

ADMINS = (
    ('Todd Davis', 'tdavis@anpi.com'),
)

MANAGERS = ADMINS

