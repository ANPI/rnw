from .base import *  # noqa


ADMINS = (
    ('Todd Davis', 'tdavis@anpi.com'),
)

MANAGERS = ADMINS


# You might want to use sqlite3 for testing in local as it's much faster.
if IN_TESTING:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': '/tmp/rnwsite_test.db',
        }
    }
