from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/rnw/')),
    url(r'^rnw/', include("rnw.urls", namespace="rnw")),
    url(r'^admin/', admin.site.urls),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^docs/', include('rest_framework_swagger.urls')),
]
