#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    cwd = os.getcwd()
    parent = os.path.dirname(cwd)
    sys.path.append(parent)

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
