=============================
RightNowWeb
=============================

Application to view exported RightNowWeb data.

Documentation
-------------

The full documentation is at https://rnw.anpi.org/help.

Quickstart
----------

Install rnwapp::

    pip install rnwapp

Then use it in a project::

    import rnw
