django>=1.8.0
wheel==0.24.0
# Additional requirements go here
djangorestframework
django-rest-swagger
django_extensions
django-vanilla-views
django-filter
django-grappelli
django-environ
sphinx-swagger
sphinxcontrib-httpdomain

