============
Installation
============

At the command line::

    $ easy_install rnwapp

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv rnwapp
    $ pip install rnwapp
