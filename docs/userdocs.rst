Using the Application
========================

This application allows the user to view data that was exported from the
RightNowWeb service.  The best way to view the data is to locate the
organization needing to be viewed.  Typing in the search box on the
organization page will search for the lookup name.

Once the organization has been located, clicking on it will bring up the
organization details.  The details page contains the following tabs:

    **Contacts**
        A list of contacts assigned to this organization.  Clicking on a
        contact will bring up details of the contact.
    **Notes**
        Notes attached to this organization.
    **Incidents**
        A list of incidents for this organization.  These too can be clicked on
        to view the details of the incident.
    **Attachments**
        A list of attachments, contracts, etc. for this organization.
