# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

from .settings import RNW_API

MEDIA_URL = settings.MEDIA_URL

class Account(models.Model):
    accountid = models.IntegerField(db_column='AccountID', primary_key=True)  # Field name made lowercase.
    lookupname = models.CharField(db_column='LookupName', max_length=255)  # Field name made lowercase.
    createdtime = models.DateTimeField(db_column='CreatedTime')  # Field name made lowercase.
    updatedtime = models.DateTimeField(db_column='UpdatedTime')  # Field name made lowercase.
    countryid = models.ForeignKey('Country', models.DO_NOTHING, db_column='CountryID')  # Field name made lowercase.
    displayname = models.CharField(db_column='DisplayName', max_length=80, blank=True, null=True)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='DisplayOrder')  # Field name made lowercase.
    emailnotificationid = models.ForeignKey('Emailnotification', models.DO_NOTHING, db_column='EmailNotificationID', blank=True, null=True)  # Field name made lowercase.
    login = models.CharField(db_column='Login', max_length=80)  # Field name made lowercase.
    managerid = models.ForeignKey('Manager', models.DO_NOTHING, db_column='ManagerID', blank=True, null=True)  # Field name made lowercase.
    firstname = models.CharField(db_column='FirstName', max_length=80)  # Field name made lowercase.
    lastname = models.CharField(db_column='LastName', max_length=80)  # Field name made lowercase.
    notificationpending = models.IntegerField(db_column='NotificationPending')  # Field name made lowercase.
    passwordexpirationtime = models.DateTimeField(db_column='PasswordExpirationTime', blank=True, null=True)  # Field name made lowercase.
    profileid = models.ForeignKey('Profile', models.DO_NOTHING, db_column='ProfileID', blank=True, null=True)  # Field name made lowercase.
    signature = models.CharField(db_column='Signature', max_length=1500, blank=True, null=True)  # Field name made lowercase.
    staffgroupid = models.ForeignKey('Staffgroup', models.DO_NOTHING, db_column='StaffGroupID')  # Field name made lowercase.
    officephone = models.CharField(db_column='OfficePhone', max_length=40, blank=True, null=True)  # Field name made lowercase.
    alternatephone1 = models.CharField(db_column='AlternatePhone1', max_length=40, blank=True, null=True)  # Field name made lowercase.
    alternatephone2 = models.CharField(db_column='AlternatePhone2', max_length=40, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Account'


class Active(models.Model):
    activeid = models.IntegerField(db_column='ActiveID', primary_key=True)  # Field name made lowercase.
    active = models.CharField(db_column='Active', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Active'


class Agent(models.Model):
    agentid = models.IntegerField(db_column='AgentID', primary_key=True)  # Field name made lowercase.
    agent = models.CharField(db_column='Agent', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Agent'


class Alttermcarrier(models.Model):
    alttermcarrierid = models.IntegerField(db_column='AltTermCarrierID', primary_key=True)  # Field name made lowercase.
    alttermcarrier = models.CharField(db_column='AltTermCarrier', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AltTermCarrier'


class Answer(models.Model):
    answerid = models.IntegerField(db_column='AnswerID', primary_key=True)  # Field name made lowercase.
    lookupname = models.CharField(db_column='LookupName', max_length=255, blank=True, null=True)  # Field name made lowercase.
    createdtime = models.DateTimeField(db_column='CreatedTime', blank=True, null=True)  # Field name made lowercase.
    updatedtime = models.DateTimeField(db_column='UpdatedTime', blank=True, null=True)  # Field name made lowercase.
    accesslevels = models.CharField(db_column='AccessLevels', max_length=25, blank=True, null=True)  # Field name made lowercase.
    adminlastaccesstime = models.DateTimeField(db_column='AdminLastAccessTime', blank=True, null=True)  # Field name made lowercase.
    answertypeid = models.ForeignKey('Answertype', models.DO_NOTHING, db_column='AnswerTypeID', blank=True, null=True)  # Field name made lowercase.
    comment = models.CharField(db_column='Comment', max_length=128, blank=True, null=True)  # Field name made lowercase.
    commoncomment = models.CharField(db_column='CommonComment', max_length=2500, blank=True, null=True)  # Field name made lowercase.
    expiresdate = models.DateTimeField(db_column='ExpiresDate', blank=True, null=True)  # Field name made lowercase.
    guidedassistanceid = models.ForeignKey('Guidedassistance', models.DO_NOTHING, db_column='GuidedAssistanceID', blank=True, null=True)  # Field name made lowercase.
    keywords = models.CharField(db_column='Keywords', max_length=1500, blank=True, null=True)  # Field name made lowercase.
    languageid = models.ForeignKey('Language', models.DO_NOTHING, db_column='LanguageID', blank=True, null=True)  # Field name made lowercase.
    lastaccesstime = models.DateTimeField(db_column='LastAccessTime', blank=True, null=True)  # Field name made lowercase.
    lastnotificationtime = models.DateTimeField(db_column='LastNotificationTime', blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=20, blank=True, null=True)  # Field name made lowercase.
    nextnotificationtime = models.DateTimeField(db_column='NextNotificationTime', blank=True, null=True)  # Field name made lowercase.
    originalreferencenumber = models.CharField(db_column='OriginalReferenceNumber', max_length=15, blank=True, null=True)  # Field name made lowercase.
    positioninlistid = models.ForeignKey('Positioninlist', models.DO_NOTHING, db_column='PositionInListID', blank=True, null=True)  # Field name made lowercase.
    publishondate = models.DateTimeField(db_column='PublishOnDate', blank=True, null=True)  # Field name made lowercase.
    statusid = models.ForeignKey('Status', models.DO_NOTHING, db_column='StatusID', blank=True, null=True)  # Field name made lowercase.
    statustypeid = models.ForeignKey('Statustype', models.DO_NOTHING, db_column='StatusTypeID', blank=True, null=True)  # Field name made lowercase.
    summary = models.CharField(db_column='Summary', max_length=255, blank=True, null=True)  # Field name made lowercase.
    updatedbyaccountid = models.ForeignKey(Account, models.DO_NOTHING, db_column='UpdatedByAccountID', blank=True, null=True)  # Field name made lowercase.
    url = models.CharField(db_column='URL', max_length=1500, blank=True, null=True)  # Field name made lowercase.
    question = models.TextField(db_column='Question', blank=True, null=True)  # Field name made lowercase.
    solution = models.TextField(db_column='Solution', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Answer'


class Answertype(models.Model):
    answertypeid = models.IntegerField(db_column='AnswerTypeID', primary_key=True)  # Field name made lowercase.
    answertype = models.CharField(db_column='AnswerType', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AnswerType'


class Asset(models.Model):
    assetid = models.IntegerField(db_column='AssetID', primary_key=True)  # Field name made lowercase.
    asset = models.CharField(db_column='Asset', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Asset'


class Attachment(models.Model):
    attachmentid = models.IntegerField(db_column='AttachmentID', primary_key=True)  # Field name made lowercase.
    incidentid = models.ForeignKey('Incident', models.DO_NOTHING, db_column='IncidentID', blank=True, null=True)  # Field name made lowercase.
    organizationid = models.ForeignKey('Organization', models.DO_NOTHING, db_column='OrganizationID', blank=True, null=True)  # Field name made lowercase.
    contactid = models.ForeignKey('Contact', models.DO_NOTHING, db_column='ContactID', blank=True, null=True)  # Field name made lowercase.
    answerid = models.ForeignKey(Answer, models.DO_NOTHING, db_column='AnswerID', blank=True, null=True)  # Field name made lowercase.
    contenttype = models.CharField(db_column='ContentType', max_length=128)  # Field name made lowercase.
    size = models.IntegerField(db_column='Size')  # Field name made lowercase.
    filename = models.CharField(db_column='FileName', max_length=128)  # Field name made lowercase.
    displayname = models.CharField(db_column='DisplayName', max_length=128, blank=True, null=True)  # Field name made lowercase.
    actualfilename = models.CharField(db_column='ActualFileName', max_length=255, blank=True, null=True)  # Field name made lowercase.
    filestatusid = models.ForeignKey('Filestatus', models.DO_NOTHING, db_column='FileStatusID')  # Field name made lowercase.

    def __str__(self):
        return self.filename

    def attachment_url(self):
        return '%s%s' % (MEDIA_URL, self.actualfilename)

    class Meta:
        managed = False
        db_table = 'Attachment'


class Csm(models.Model):
    csmid = models.IntegerField(db_column='CSMID', primary_key=True)  # Field name made lowercase.
    csm = models.CharField(db_column='CSM', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CSM'


class Calltype(models.Model):
    calltypeid = models.IntegerField(db_column='CallTypeID', primary_key=True)  # Field name made lowercase.
    calltype = models.CharField(db_column='CallType', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CallType'


class Carrier(models.Model):
    carrierid = models.IntegerField(db_column='CarrierID', primary_key=True)  # Field name made lowercase.
    carrier = models.CharField(db_column='Carrier', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Carrier'


class Causecode(models.Model):
    causecodeid = models.IntegerField(db_column='CauseCodeID', primary_key=True)  # Field name made lowercase.
    causecode = models.CharField(db_column='CauseCode', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CauseCode'


class Channel(models.Model):
    channelid = models.IntegerField(db_column='ChannelID', primary_key=True)  # Field name made lowercase.
    channel = models.CharField(db_column='Channel', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Channel'


class Chatqueue(models.Model):
    chatqueueid = models.IntegerField(db_column='ChatQueueID', primary_key=True)  # Field name made lowercase.
    chatqueue = models.CharField(db_column='ChatQueue', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ChatQueue'


class Company(models.Model):
    companyid = models.IntegerField(db_column='CompanyID', primary_key=True)  # Field name made lowercase.
    company = models.CharField(db_column='Company', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Company'


class Contact(models.Model):
    contactid = models.IntegerField(db_column='ContactID', primary_key=True)  # Field name made lowercase.
    lookupname = models.CharField(db_column='LookupName', max_length=255)  # Field name made lowercase.
    createdtime = models.DateTimeField(db_column='CreatedTime')  # Field name made lowercase.
    updatedtime = models.DateTimeField(db_column='UpdatedTime')  # Field name made lowercase.
    contacttypeid = models.ForeignKey('Contacttype', models.DO_NOTHING, db_column='ContactTypeID', blank=True, null=True)  # Field name made lowercase.
    crmmarketing = models.IntegerField(db_column='CRMMarketing')  # Field name made lowercase.
    crmsales = models.IntegerField(db_column='CRMSales')  # Field name made lowercase.
    crmservice = models.IntegerField(db_column='CRMService')  # Field name made lowercase.
    disabled = models.IntegerField(db_column='Disabled')  # Field name made lowercase.
    login = models.CharField(db_column='Login', max_length=80, blank=True, null=True)  # Field name made lowercase.
    firstname = models.CharField(db_column='FirstName', max_length=80)  # Field name made lowercase.
    lastname = models.CharField(db_column='LastName', max_length=80)  # Field name made lowercase.
    organizationid = models.ForeignKey('Organization', models.DO_NOTHING, db_column='OrganizationID', blank=True, null=True)  # Field name made lowercase.
    passwordemailexpirationtime = models.DateTimeField(db_column='PasswordEmailExpirationTime', blank=True, null=True)  # Field name made lowercase.
    passwordexpirationtime = models.DateTimeField(db_column='PasswordExpirationTime', blank=True, null=True)  # Field name made lowercase.
    sourceid = models.ForeignKey('Source', models.DO_NOTHING, db_column='SourceID')  # Field name made lowercase.
    title = models.IntegerField(db_column='Title', blank=True, null=True)  # Field name made lowercase.
    phonenumber = models.CharField(db_column='PhoneNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    membernumber = models.CharField(db_column='MemberNumber', max_length=8, blank=True, null=True)  # Field name made lowercase.
    referralcode = models.CharField(db_column='ReferralCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    classofservice = models.CharField(db_column='ClassOfService', max_length=10, blank=True, null=True)  # Field name made lowercase.
    lastbill = models.CharField(db_column='LastBill', max_length=15, blank=True, null=True)  # Field name made lowercase.
    usage = models.CharField(db_column='Usage', max_length=15, blank=True, null=True)  # Field name made lowercase.
    companyid = models.ForeignKey(Company, models.DO_NOTHING, db_column='CompanyID', blank=True, null=True)  # Field name made lowercase.
    mobilenumber = models.CharField(db_column='MobileNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    contacthours = models.CharField(db_column='ContactHours', max_length=4000, blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        return self.lookupname

    def get_api_url(self):
        return "%s/contact/%s" % (RNW_API, self.contactid)

    class Meta:
        managed = False
        db_table = 'Contact'


class Contacttype(models.Model):
    contacttypeid = models.IntegerField(db_column='ContactTypeID', primary_key=True)  # Field name made lowercase.
    contacttype = models.CharField(db_column='ContactType', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ContactType'


class Contenttype(models.Model):
    contenttypeid = models.IntegerField(db_column='ContentTypeID', primary_key=True)  # Field name made lowercase.
    contenttype = models.CharField(db_column='ContentType', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ContentType'


class Country(models.Model):
    countryid = models.IntegerField(db_column='CountryID', primary_key=True)  # Field name made lowercase.
    country = models.CharField(db_column='Country', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Country'


class Division(models.Model):
    divisionid = models.IntegerField(db_column='DivisionID', primary_key=True)  # Field name made lowercase.
    division = models.CharField(db_column='Division', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Division'


class Egressswitch(models.Model):
    egressswitchid = models.IntegerField(db_column='EgressSwitchID', primary_key=True)  # Field name made lowercase.
    egressswitch = models.CharField(db_column='EgressSwitch', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EgressSwitch'


class Emailnotification(models.Model):
    emailnotificationid = models.IntegerField(db_column='EmailNotificationID', primary_key=True)  # Field name made lowercase.
    emailnotification = models.CharField(db_column='EmailNotification', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EmailNotification'


class Entrytype(models.Model):
    entrytypeid = models.IntegerField(db_column='EntryTypeID', primary_key=True)  # Field name made lowercase.
    entrytype = models.CharField(db_column='EntryType', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EntryType'


class Filestatus(models.Model):
    filestatusid = models.IntegerField(db_column='FileStatusID', primary_key=True)  # Field name made lowercase.
    filestatus = models.CharField(db_column='FileStatus', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FileStatus'


class Guidedassistance(models.Model):
    guidedassistanceid = models.IntegerField(db_column='GuidedAssistanceID', primary_key=True)  # Field name made lowercase.
    guidedassistance = models.CharField(db_column='GuidedAssistance', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'GuidedAssistance'


class Incident(models.Model):
    incidentid = models.IntegerField(db_column='IncidentID', primary_key=True)  # Field name made lowercase.
    lookupname = models.CharField(db_column='LookupName', max_length=255)  # Field name made lowercase.
    createdtime = models.DateTimeField(db_column='CreatedTime')  # Field name made lowercase.
    updatedtime = models.DateTimeField(db_column='UpdatedTime')  # Field name made lowercase.
    assetid = models.ForeignKey(Asset, models.DO_NOTHING, db_column='AssetID', blank=True, null=True)  # Field name made lowercase.
    assignedtoid = models.ForeignKey(Account, models.DO_NOTHING, db_column='AssignedToID', blank=True, null=True)  # Field name made lowercase.
    assignedtogroupid = models.ForeignKey('Staffgroup', models.DO_NOTHING, db_column='AssignedToGroupID', blank=True, null=True)  # Field name made lowercase.
    categoryid = models.ForeignKey('Servicecategory', models.DO_NOTHING, db_column='CategoryID', blank=True, null=True)  # Field name made lowercase.
    channelid = models.ForeignKey(Channel, models.DO_NOTHING, db_column='ChannelID', blank=True, null=True)  # Field name made lowercase.
    chatqueueid = models.ForeignKey(Chatqueue, models.DO_NOTHING, db_column='ChatQueueID', blank=True, null=True)  # Field name made lowercase.
    closedtime = models.DateTimeField(db_column='ClosedTime', blank=True, null=True)  # Field name made lowercase.
    createdbyaccountid = models.ForeignKey(Account, models.DO_NOTHING, related_name='+', db_column='CreatedByAccountID', blank=True, null=True)  # Field name made lowercase.
    dispositionid = models.ForeignKey('Servicedisposition', models.DO_NOTHING, db_column='DispositionID', blank=True, null=True)  # Field name made lowercase.
    initialresponseduetime = models.DateTimeField(db_column='InitialResponseDueTime', blank=True, null=True)  # Field name made lowercase.
    initialsolutiontime = models.DateTimeField(db_column='InitialSolutionTime', blank=True, null=True)  # Field name made lowercase.
    interfaceid = models.ForeignKey('Interface', models.DO_NOTHING, db_column='InterfaceID')  # Field name made lowercase.
    languageid = models.ForeignKey('Language', models.DO_NOTHING, db_column='LanguageID', blank=True, null=True)  # Field name made lowercase.
    lastresponsetime = models.DateTimeField(db_column='LastResponseTime', blank=True, null=True)  # Field name made lowercase.
    mailboxid = models.ForeignKey('Servicemailbox', models.DO_NOTHING, db_column='MailboxID', blank=True, null=True)  # Field name made lowercase.
    mailingid = models.ForeignKey('Mailing', models.DO_NOTHING, db_column='MailingID', blank=True, null=True)  # Field name made lowercase.
    organizationid = models.ForeignKey('Organization', models.DO_NOTHING, db_column='OrganizationID', blank=True, null=True)  # Field name made lowercase.
    productid = models.ForeignKey('Serviceproduct', models.DO_NOTHING, db_column='ProductID', blank=True, null=True)  # Field name made lowercase.
    queueid = models.ForeignKey('Queue', models.DO_NOTHING, db_column='QueueID', blank=True, null=True)  # Field name made lowercase.
    referencenumber = models.CharField(db_column='ReferenceNumber', max_length=13)  # Field name made lowercase.
    resolutioninterval = models.IntegerField(db_column='ResolutionInterval', blank=True, null=True)  # Field name made lowercase.
    responseemailaddresstypeid = models.ForeignKey('Responseemailaddresstype', models.DO_NOTHING, db_column='ResponseEmailAddressTypeID')  # Field name made lowercase.
    responseinterval = models.IntegerField(db_column='ResponseInterval', blank=True, null=True)  # Field name made lowercase.
    severityid = models.ForeignKey('Severity', models.DO_NOTHING, db_column='SeverityID', blank=True, null=True)  # Field name made lowercase.
    smartsensecustomer = models.IntegerField(db_column='SmartSenseCustomer', blank=True, null=True)  # Field name made lowercase.
    smartsensestaff = models.IntegerField(db_column='SmartSenseStaff', blank=True, null=True)  # Field name made lowercase.
    sourceid = models.ForeignKey('Source', models.DO_NOTHING, db_column='SourceID')  # Field name made lowercase.
    statusid = models.ForeignKey('Status', models.DO_NOTHING, db_column='StatusID')  # Field name made lowercase.
    statustypeid = models.ForeignKey('Statustype', models.DO_NOTHING, db_column='StatusTypeID')  # Field name made lowercase.
    subject = models.CharField(db_column='Subject', max_length=512, blank=True, null=True)  # Field name made lowercase.
    carrierid = models.ForeignKey(Carrier, models.DO_NOTHING, db_column='CarrierID', blank=True, null=True)  # Field name made lowercase.
    issueid = models.ForeignKey('Issue', models.DO_NOTHING, db_column='IssueID', blank=True, null=True)  # Field name made lowercase.
    resolutionid = models.ForeignKey('Resolution', models.DO_NOTHING, db_column='ResolutionID', blank=True, null=True)  # Field name made lowercase.
    carrierticketnumber = models.CharField(db_column='CarrierTicketNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
    calltypeid = models.ForeignKey(Calltype, models.DO_NOTHING, db_column='CallTypeID', blank=True, null=True)  # Field name made lowercase.
    carrierttopendate = models.DateTimeField(db_column='CarrierTTOpenDate', blank=True, null=True)  # Field name made lowercase.
    causecodeid = models.ForeignKey(Causecode, models.DO_NOTHING, db_column='CauseCodeID', blank=True, null=True)  # Field name made lowercase.
    orignumber = models.CharField(db_column='OrigNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    termnumber = models.CharField(db_column='TermNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    primarytermcarrierid = models.ForeignKey('Primarytermcarrier', models.DO_NOTHING, db_column='PrimaryTermCarrierID', blank=True, null=True)  # Field name made lowercase.
    alttermcarrierid = models.ForeignKey(Alttermcarrier, models.DO_NOTHING, db_column='AltTermCarrierID', blank=True, null=True)  # Field name made lowercase.
    resolutiondate = models.DateTimeField(db_column='ResolutionDate', blank=True, null=True)  # Field name made lowercase.
    callsampledate = models.DateTimeField(db_column='CallSampleDate', blank=True, null=True)  # Field name made lowercase.
    circuitidnumber = models.CharField(db_column='CircuitIDNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
    retestresultsid = models.ForeignKey('Retestresults', models.DO_NOTHING, db_column='RetestResultsID', blank=True, null=True)  # Field name made lowercase.
    jurisdictionid = models.ForeignKey('Jurisdiction', models.DO_NOTHING, db_column='JurisdictionID', blank=True, null=True)  # Field name made lowercase.
    whitegloveid = models.ForeignKey('Whiteglove', models.DO_NOTHING, db_column='WhiteGloveID', blank=True, null=True)  # Field name made lowercase.
    networkid = models.ForeignKey('Network', models.DO_NOTHING, db_column='NetworkID', blank=True, null=True)  # Field name made lowercase.
    servicelevelid = models.ForeignKey('Servicelevel', models.DO_NOTHING, db_column='ServiceLevelID', blank=True, null=True)  # Field name made lowercase.
    ingresstrunkgroup = models.CharField(db_column='IngressTrunkGroup', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    ocnold = models.CharField(db_column='OCNOld', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    lataold = models.CharField(db_column='LATAOld', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    egresstrunkgroup = models.CharField(db_column='EgressTrunkGroup', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    ingressold = models.CharField(db_column='IngressOld', max_length=25, blank=True, null=True)  # Field name made lowercase.
    actiontaken = models.CharField(db_column='ActionTaken', max_length=50, blank=True, null=True)  # Field name made lowercase.
    origstateid = models.ForeignKey('Origstate', models.DO_NOTHING, db_column='OrigStateID', blank=True, null=True)  # Field name made lowercase.
    termstateid = models.ForeignKey('Termstate', models.DO_NOTHING, db_column='TermStateID', blank=True, null=True)  # Field name made lowercase.
    circuitnumber = models.CharField(db_column='CircuitNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ocn = models.CharField(db_column='OCN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    lata = models.CharField(db_column='LATA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ingressswitchid = models.ForeignKey('Ingressswitch', models.DO_NOTHING, db_column='IngressSwitchID', blank=True, null=True)  # Field name made lowercase.
    egressswitchid = models.ForeignKey(Egressswitch, models.DO_NOTHING, db_column='EgressSwitchID', blank=True, null=True)  # Field name made lowercase.
    contactid = models.ForeignKey(Contact, models.DO_NOTHING, db_column='ContactID', blank=True, null=True)  # Field name made lowercase.

    def get_api_url(self):
        return "%s/incidents/%s" % (RNW_API, self.incidentid)

    def __str__(self):
        return "%s: %s" % (self.incidentid, self.subject)

    class Meta:
        managed = False
        db_table = 'Incident'


class Incidentcustomfields(models.Model):
    incidentid = models.ForeignKey(Incident, models.DO_NOTHING, db_column='IncidentID')  # Field name made lowercase.
    carrierid = models.IntegerField(db_column='CarrierID', blank=True, null=True)  # Field name made lowercase.
    issueid = models.IntegerField(db_column='IssueID', blank=True, null=True)  # Field name made lowercase.
    resolutionid = models.IntegerField(db_column='ResolutionID', blank=True, null=True)  # Field name made lowercase.
    carrierticketnumber = models.CharField(db_column='CarrierTicketNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
    calltypeid = models.IntegerField(db_column='CallTypeID', blank=True, null=True)  # Field name made lowercase.
    carrierttopendate = models.DateTimeField(db_column='CarrierTTOpenDate', blank=True, null=True)  # Field name made lowercase.
    causecodeid = models.IntegerField(db_column='CauseCodeID', blank=True, null=True)  # Field name made lowercase.
    orignumber = models.CharField(db_column='OrigNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    termnumber = models.CharField(db_column='TermNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    primarytermcarrierid = models.IntegerField(db_column='PrimaryTermCarrierID', blank=True, null=True)  # Field name made lowercase.
    alttermcarrierid = models.IntegerField(db_column='AltTermCarrierID', blank=True, null=True)  # Field name made lowercase.
    resolutiondate = models.DateTimeField(db_column='ResolutionDate', blank=True, null=True)  # Field name made lowercase.
    callsampledate = models.DateTimeField(db_column='CallSampleDate', blank=True, null=True)  # Field name made lowercase.
    circuitidnumber = models.CharField(db_column='CircuitIDNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
    retestresultsid = models.IntegerField(db_column='RetestResultsID', blank=True, null=True)  # Field name made lowercase.
    jurisdictionid = models.IntegerField(db_column='JurisdictionID', blank=True, null=True)  # Field name made lowercase.
    whitegloveid = models.IntegerField(db_column='WhiteGloveID', blank=True, null=True)  # Field name made lowercase.
    networkid = models.IntegerField(db_column='NetworkID', blank=True, null=True)  # Field name made lowercase.
    servicelevelid = models.IntegerField(db_column='ServiceLevelID', blank=True, null=True)  # Field name made lowercase.
    ingresstrunkgroup = models.CharField(db_column='IngressTrunkGroup', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    ocnold = models.CharField(db_column='OCNOld', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    lataold = models.CharField(db_column='LATAOld', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    egresstrunkgroup = models.CharField(db_column='EgressTrunkGroup', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    ingressold = models.CharField(db_column='IngressOld', max_length=25, blank=True, null=True)  # Field name made lowercase.
    actiontaken = models.CharField(db_column='ActionTaken', max_length=50, blank=True, null=True)  # Field name made lowercase.
    origstateid = models.IntegerField(db_column='OrigStateID', blank=True, null=True)  # Field name made lowercase.
    termstateid = models.IntegerField(db_column='TermStateID', blank=True, null=True)  # Field name made lowercase.
    circuitnumber = models.CharField(db_column='CircuitNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ocn = models.CharField(db_column='OCN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    lata = models.CharField(db_column='LATA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ingressswitchid = models.IntegerField(db_column='IngressSwitchID', blank=True, null=True)  # Field name made lowercase.
    egressswitchid = models.IntegerField(db_column='EgressSwitchID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IncidentCustomFields'


class Incidentprimarycontact(models.Model):
    incidentid = models.ForeignKey(Incident, models.DO_NOTHING, db_column='IncidentID')  # Field name made lowercase.
    primarycontactid = models.IntegerField(db_column='PrimaryContactID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IncidentPrimaryContact'


class IncidentThreads(models.Model):
    threadid = models.IntegerField(db_column='ThreadID', primary_key=True)  # Field name made lowercase.
    incidentid = models.ForeignKey(Incident, models.DO_NOTHING, db_column='IncidentID')  # Field name made lowercase.
    createdtime = models.DateTimeField(db_column='CreatedTime', blank=True, null=True)  # Field name made lowercase.
    entrytypeid = models.ForeignKey(Entrytype, models.DO_NOTHING, db_column='EntryTypeID')  # Field name made lowercase.
    contenttypeid = models.ForeignKey(Contenttype, models.DO_NOTHING, db_column='ContentTypeID')  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='DisplayOrder')  # Field name made lowercase.
    accountid = models.ForeignKey(Account, models.DO_NOTHING, db_column='AccountID', blank=True, null=True)  # Field name made lowercase.
    channelid = models.ForeignKey(Channel, models.DO_NOTHING, db_column='ChannelID', blank=True, null=True)  # Field name made lowercase.
    contactid = models.ForeignKey(Contact, models.DO_NOTHING, db_column='ContactID', blank=True, null=True)  # Field name made lowercase.
    mailheader = models.TextField(db_column='MailHeader', blank=True, null=True)  # Field name made lowercase.
    text = models.TextField(db_column='Text')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IncidentThreads'
        ordering = ['incidentid', 'threadid']


class Industry(models.Model):
    industryid = models.IntegerField(db_column='IndustryID', primary_key=True)  # Field name made lowercase.
    industry = models.CharField(db_column='Industry', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Industry'


class Ingressswitch(models.Model):
    ingressswitchid = models.IntegerField(db_column='IngressSwitchID', primary_key=True)  # Field name made lowercase.
    ingressswitch = models.CharField(db_column='IngressSwitch', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IngressSwitch'


class Interface(models.Model):
    interfaceid = models.IntegerField(db_column='InterfaceID', primary_key=True)  # Field name made lowercase.
    interface = models.CharField(db_column='Interface', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Interface'


class Internalsalesmgr(models.Model):
    internalsalesmgrid = models.IntegerField(db_column='InternalSalesMgrID', primary_key=True)  # Field name made lowercase.
    internalsalesmgr = models.CharField(db_column='InternalSalesMgr', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'InternalSalesMgr'


class Issue(models.Model):
    issueid = models.IntegerField(db_column='IssueID', primary_key=True)  # Field name made lowercase.
    issue = models.CharField(db_column='Issue', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Issue'


class Jurisdiction(models.Model):
    jurisdictionid = models.IntegerField(db_column='JurisdictionID', primary_key=True)  # Field name made lowercase.
    jurisdiction = models.CharField(db_column='Jurisdiction', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Jurisdiction'


class Language(models.Model):
    languageid = models.IntegerField(db_column='LanguageID', primary_key=True)  # Field name made lowercase.
    language = models.CharField(db_column='Language', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Language'


class Mailing(models.Model):
    mailingid = models.IntegerField(db_column='MailingID', primary_key=True)  # Field name made lowercase.
    mailing = models.CharField(db_column='Mailing', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Mailing'


class Manager(models.Model):
    managerid = models.IntegerField(db_column='ManagerID', primary_key=True)  # Field name made lowercase.
    manager = models.CharField(db_column='Manager', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Manager'


class Network(models.Model):
    networkid = models.IntegerField(db_column='NetworkID', primary_key=True)  # Field name made lowercase.
    network = models.CharField(db_column='Network', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Network'


class Note(models.Model):
    noteid = models.IntegerField(db_column='NoteID', primary_key=True)  # Field name made lowercase.
    organizationid = models.ForeignKey('Organization', models.DO_NOTHING, db_column='OrganizationID', blank=True, null=True)  # Field name made lowercase.
    contactid = models.ForeignKey(Contact, models.DO_NOTHING, db_column='ContactID', blank=True, null=True)  # Field name made lowercase.
    channelid = models.ForeignKey(Channel, models.DO_NOTHING, db_column='ChannelID', blank=True, null=True)  # Field name made lowercase.
    createdbyaccountid = models.ForeignKey(Account, models.DO_NOTHING, related_name='created_notes', db_column='CreatedByAccountID', blank=True, null=True)  # Field name made lowercase.
    createdtime = models.DateTimeField(db_column='CreatedTime')  # Field name made lowercase.
    updatedbyaccountid = models.ForeignKey(Account, models.DO_NOTHING, db_column='UpdatedByAccountID', blank=True, null=True)  # Field name made lowercase.
    updatedtime = models.DateTimeField(db_column='UpdatedTime')  # Field name made lowercase.
    text = models.TextField(db_column='Text')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Note'


class Organization(models.Model):
    organizationid = models.IntegerField(db_column='OrganizationID', primary_key=True)  # Field name made lowercase.
    lookupname = models.CharField(db_column='LookupName', max_length=255)  # Field name made lowercase.
    createdtime = models.DateTimeField(db_column='CreatedTime')  # Field name made lowercase.
    updatedtime = models.DateTimeField(db_column='UpdatedTime')  # Field name made lowercase.
    crmmarketing = models.IntegerField(db_column='CRMMarketing')  # Field name made lowercase.
    crmsales = models.IntegerField(db_column='CRMSales')  # Field name made lowercase.
    crmservice = models.IntegerField(db_column='CRMService')  # Field name made lowercase.
    industryid = models.ForeignKey(Industry, models.DO_NOTHING, db_column='IndustryID', blank=True, null=True)  # Field name made lowercase.
    login = models.CharField(db_column='Login', max_length=40, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=80)  # Field name made lowercase.
    namefurigana = models.CharField(db_column='NameFurigana', max_length=80, blank=True, null=True)  # Field name made lowercase.
    numberofemployees = models.IntegerField(db_column='NumberOfEmployees', blank=True, null=True)  # Field name made lowercase.
    parentid = models.IntegerField(db_column='ParentID', blank=True, null=True)  # Field name made lowercase.
    salesacquireddate = models.DateTimeField(db_column='SalesAcquiredDate', blank=True, null=True)  # Field name made lowercase.
    salesaccountid = models.IntegerField(db_column='SalesAccountID', blank=True, null=True)  # Field name made lowercase.
    salestotalrevenue = models.IntegerField(db_column='SalesTotalRevenue', blank=True, null=True)  # Field name made lowercase.
    sourceid = models.ForeignKey('Source', models.DO_NOTHING, db_column='SourceID')  # Field name made lowercase.
    divisionid = models.ForeignKey(Division, models.DO_NOTHING, db_column='DivisionID', blank=True, null=True)  # Field name made lowercase.
    csmid = models.ForeignKey(Csm, models.DO_NOTHING, db_column='CSMID', blank=True, null=True)  # Field name made lowercase.
    internalsalesmgrid = models.ForeignKey(Internalsalesmgr, models.DO_NOTHING, db_column='InternalSalesMgrID', blank=True, null=True)  # Field name made lowercase.
    salesagentid = models.ForeignKey('Salesagent', models.DO_NOTHING, db_column='SalesAgentID', blank=True, null=True)  # Field name made lowercase.
    activeid = models.ForeignKey(Active, models.DO_NOTHING, db_column='ActiveID', blank=True, null=True)  # Field name made lowercase.
    agentid = models.ForeignKey(Agent, models.DO_NOTHING, db_column='AgentID', blank=True, null=True)  # Field name made lowercase.
    agent = models.CharField(db_column='Agent', max_length=50, blank=True, null=True)  # Field name made lowercase.

    def get_api_url(self):
        return "%s/organizations/%s" % (RNW_API, self.organizationid)

    def __str__(self):
        return self.lookupname

    class Meta:
        managed = False
        db_table = 'Organization'


class Origstate(models.Model):
    origstateid = models.IntegerField(db_column='OrigStateID', primary_key=True)  # Field name made lowercase.
    origstate = models.CharField(db_column='OrigState', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OrigState'


class Positioninlist(models.Model):
    positioninlistid = models.IntegerField(db_column='PositionInListID', primary_key=True)  # Field name made lowercase.
    positioninlist = models.CharField(db_column='PositionInList', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PositionInList'


class Primarytermcarrier(models.Model):
    primarytermcarrierid = models.IntegerField(db_column='PrimaryTermCarrierID', primary_key=True)  # Field name made lowercase.
    primarytermcarrier = models.CharField(db_column='PrimaryTermCarrier', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PrimaryTermCarrier'


class Profile(models.Model):
    profileid = models.IntegerField(db_column='ProfileID', primary_key=True)  # Field name made lowercase.
    profile = models.CharField(db_column='Profile', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Profile'


class Queue(models.Model):
    queueid = models.IntegerField(db_column='QueueID', primary_key=True)  # Field name made lowercase.
    queue = models.CharField(db_column='Queue', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Queue'


class Resolution(models.Model):
    resolutionid = models.IntegerField(db_column='ResolutionID', primary_key=True)  # Field name made lowercase.
    resolution = models.CharField(db_column='Resolution', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Resolution'


class Responseemailaddresstype(models.Model):
    responseemailaddresstypeid = models.IntegerField(db_column='ResponseEmailAddressTypeID', primary_key=True)  # Field name made lowercase.
    responseemailaddresstype = models.CharField(db_column='ResponseEmailAddressType', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ResponseEmailAddressType'


class Retestresults(models.Model):
    retestresultsid = models.IntegerField(db_column='RetestResultsID', primary_key=True)  # Field name made lowercase.
    retestresults = models.CharField(db_column='RetestResults', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RetestResults'


class Salesagent(models.Model):
    salesagentid = models.IntegerField(db_column='SalesAgentID', primary_key=True)  # Field name made lowercase.
    salesagent = models.CharField(db_column='SalesAgent', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SalesAgent'


class Servicecategory(models.Model):
    servicecategoryid = models.IntegerField(db_column='ServiceCategoryID', primary_key=True)  # Field name made lowercase.
    servicecategory = models.CharField(db_column='ServiceCategory', max_length=255)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='DisplayOrder')  # Field name made lowercase.
    parentid = models.IntegerField(db_column='ParentID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ServiceCategory'


class Servicedisposition(models.Model):
    servicedispositionid = models.IntegerField(db_column='ServiceDispositionID', primary_key=True)  # Field name made lowercase.
    servicedisposition = models.CharField(db_column='ServiceDisposition', max_length=255)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='DisplayOrder')  # Field name made lowercase.
    parentid = models.IntegerField(db_column='ParentID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ServiceDisposition'


class Servicelevel(models.Model):
    servicelevelid = models.IntegerField(db_column='ServiceLevelID', primary_key=True)  # Field name made lowercase.
    servicelevel = models.CharField(db_column='ServiceLevel', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ServiceLevel'


class Servicemailbox(models.Model):
    servicemailboxid = models.IntegerField(db_column='ServiceMailboxID', primary_key=True)  # Field name made lowercase.
    servicemailbox = models.CharField(db_column='ServiceMailbox', max_length=255)  # Field name made lowercase.
    interfaceid = models.IntegerField(db_column='InterfaceID')  # Field name made lowercase.
    isdefault = models.TextField(db_column='IsDefault')  # Field name made lowercase. This field type is a guess.
    typeid = models.IntegerField(db_column='TypeID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ServiceMailbox'


class Serviceproduct(models.Model):
    serviceproductid = models.IntegerField(db_column='ServiceProductID', primary_key=True)  # Field name made lowercase.
    serviceproduct = models.CharField(db_column='ServiceProduct', max_length=255)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='DisplayOrder')  # Field name made lowercase.
    parentid = models.IntegerField(db_column='ParentID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ServiceProduct'


class Severity(models.Model):
    severityid = models.IntegerField(db_column='SeverityID', primary_key=True)  # Field name made lowercase.
    severity = models.CharField(db_column='Severity', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Severity'


class Source(models.Model):
    sourceid = models.IntegerField(db_column='SourceID', primary_key=True)  # Field name made lowercase.
    source = models.CharField(db_column='Source', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Source'


class Staffgroup(models.Model):
    staffgroupid = models.IntegerField(db_column='StaffGroupID', primary_key=True)  # Field name made lowercase.
    staffgroup = models.CharField(db_column='StaffGroup', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'StaffGroup'


class Standardcontent(models.Model):
    standardcontentid = models.IntegerField(db_column='StandardContentID', primary_key=True)  # Field name made lowercase.
    standardcontent = models.CharField(db_column='StandardContent', max_length=255)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='DisplayOrder')  # Field name made lowercase.
    hotkey = models.CharField(db_column='HotKey', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'StandardContent'


class Status(models.Model):
    statusid = models.IntegerField(db_column='StatusID', primary_key=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Status'


class Statustype(models.Model):
    statustypeid = models.IntegerField(db_column='StatusTypeID', primary_key=True)  # Field name made lowercase.
    statustype = models.CharField(db_column='StatusType', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'StatusType'


class Termstate(models.Model):
    termstateid = models.IntegerField(db_column='TermStateID', primary_key=True)  # Field name made lowercase.
    termstate = models.CharField(db_column='TermState', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TermState'


class Whiteglove(models.Model):
    whitegloveid = models.IntegerField(db_column='WhiteGloveID', primary_key=True)  # Field name made lowercase.
    whiteglove = models.CharField(db_column='WhiteGlove', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'WhiteGlove'


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'
