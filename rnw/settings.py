
from django.conf import settings 

if hasattr(settings,'RNW_API'):
    RNW_API = settings.RNW_API
else:
    RNW_API = '/rnw/api'
