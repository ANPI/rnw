from django.contrib import admin
from .models import Incident, IncidentThreads

class ThreadInline(admin.TabularInline):
    model = IncidentThreads
    fields = ['account', 'createdtime', 'text']

@admin.register(Incident)
class IncidentAdmin(admin.ModelAdmin):
    inlines = [ ThreadInline, ]
