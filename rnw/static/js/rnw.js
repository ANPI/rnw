var Navbar = ReactBootstrap.Navbar;
var Nav = ReactBootstrap.Nav;
var NavItem = ReactBootstrap.NavItem;
var Tabs = ReactBootstrap.Tabs;
var Tab = ReactBootstrap.Tab;
var Panel = ReactBootstrap.Panel;
var Button = ReactBootstrap.Button;
var Modal = ReactBootstrap.Modal;
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var Input = ReactBootstrap.Input;
var Pagination = ReactBootstrap.Pagination;

var FormControls = ReactBootstrap.FormControls;
var ReactBsTable = window.BootstrapTable;

var NavBar = React.createClass({
    handleNav(selectedKey) {
      if (selectedKey == 0) {
        React.render(<OrgSearchComponent source="/rnw/api/organizations"/>, document.getElementById('tabbed'));
    } else if (selectedKey == 1) {
        React.render(<IncidentList source='/rnw/api/incidents/' />
, document.getElementById('tabbed'));
    }

    },
    render: function () {

        // use MAP to create LI elements from our navItems prop
        var navElements = this.props.navItems.map( function(title, index) {
            var navItem = <NavItem eventKey={index} href="#">{ title }</NavItem>
            return navItem;
        });

        // we now draw our 'navElements' inside the UL
        return (
            <Navbar fluid={true}>
              <Nav onSelect={this.handleNav}>
                { navElements }
              </Nav>
            </Navbar>
        );
    }
});
var SiteHeader = React.createClass({
    render: function () {
            var taglineEl = false;
            if (this.props.tagline) {
                taglineEl = <h3>{ this.props.tagline }</h3>;
            }
            var imageEl = false;
            if (this.props.imgSrc) {
                imageEl = <img src={ this.props.imgSrc } alt={ this.props.imgAlt } />;
            }
            // make our usual H1 tag
            var h1El = <h1>{ this.props.headerText }</h1>;
            if (imageEl && this.props.preferImageToH1) {
                h1El = false;
            }
            return (
                <header>
                    <Grid fluid={true}>
                    <Row className="show-grid">
                    <Col md={2}>
                    { h1El }
                    { imageEl }
                    </Col>
                    <Col>
                    { taglineEl }
                    </Col>
                    </Row>
                    </Grid>
                    <NavBar navItems={ this.props.navItems } />
                </header>
            );
        }
});

var OrgComponent = React.createClass({
    render: function() {
        return (
            <Panel>
            <form className="">
    <FormControls.Static label="Organization Name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.props.organization.lookupname} />
    <FormControls.Static label="Division" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.props.organization.division}></FormControls.Static>
  </form>
  </Panel>
        );
    }
});
var OrgRow = React.createClass({
    render: function () {

    }
});
var ContactTabs = React.createClass({
      render: function() {
        return (
          <Tabs>
            <Tab eventKey={1} title="Notes">
              <NotesTab notes={this.props.contact.notes}/>
            </Tab>
            <Tab eventKey={2} title="Incidents">
              <IncidentList source='/rnw/api/incidents/' filter_field="contactid" filter_value={this.props.contact.contactid} />
            </Tab>
            <Tab eventKey={3} title="Attachments">
            <Attachments attachments={this.props.contact.attachments} />
            </Tab>
          </Tabs>
        );
      }
});

var OrgTabs = React.createClass({
      render: function() {
        return (
          <Tabs>
            <Tab eventKey={1} title="Contacts">
              <ContactList contacts={this.props.organization.contacts}/>
            </Tab>
            <Tab eventKey={2} title="Notes">
              <NotesTab notes={this.props.organization.notes}/>
            </Tab>
            <Tab eventKey={3} title="Incidents">
              <IncidentList source='/rnw/api/incidents/' filter_field="organizationid" filter_value={this.props.organization.organizationid} />
            </Tab>
            <Tab eventKey={4} title="Attachments">
              <Attachments attachments={this.props.organization.attachments} />
            </Tab>
          </Tabs>
        );
      }
});
var IncidentTabs = React.createClass({
  render: function() {
    return (
      <Tabs>
      <Tab eventKey={1} title="Messages">
        {this.props.messages}
      </Tab>
        <Tab eventKey={2} title="Attachments">
          <Attachments attachments={this.props.attachments} />
        </Tab>
      </Tabs>
    );
  }
});
var Attachments = React.createClass({
    getDefaultProps: function() {
        return {
                attachments: []
        };
    },
    open(row, isSelected) {
      var url = row.url;
      window.open(url);
    },
  render: function() {
      let selectRowProp = {
          mode: "radio",
          clickToSelect: true,
          // bgColor: "rgb(0, 193, 0)",
          onSelect: this.open
      };
    return (
      <BootstrapTable data={this.props.attachments}
        paginationSize={5}
        pagination={true}
        selectRow={selectRowProp}
        striped={true}
        hover={true}>
            <TableHeaderColumn dataField="attachmentid" isKey={true} hidden={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
            <TableHeaderColumn dataField="contenttype" width={150} hidden={true} dataSort={false}>Type</TableHeaderColumn>
            <TableHeaderColumn dataField="filename" dataSort={true} >Filename</TableHeaderColumn>
            <TableHeaderColumn dataField="size" dataSort={true} >Size</TableHeaderColumn>
            <TableHeaderColumn dataField="filestatus" dataSort={true} >Status</TableHeaderColumn>
        </BootstrapTable>
    );
  }
});
var NotesTab = React.createClass({
  render: function() {
    return (
      <BootstrapTable data={this.props.notes} paginationSize={5} pagination={true} striped={true} hover={true}>
            <TableHeaderColumn dataField="noteid" isKey={true} hidden={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
            <TableHeaderColumn dataField="channel" width={150} dataSort={true}>Channel</TableHeaderColumn>
            <TableHeaderColumn dataField="createdtime" width={200} dataSort={true} >Created</TableHeaderColumn>
            <TableHeaderColumn dataField="text" >Note</TableHeaderColumn>
        </BootstrapTable>
    );
  }
});
var IncidentDetails = React.createClass({
    getInitialState: function() {
          var incident = this.props.incident;
          incident.incidentthreads_set = [];
            return {
                    incident: incident,
            };
    },
    build_url() {
        return this.state.incident.url;
    },
    componentDidMount: function() {
      this.getData(this.build_url());
    },

    componentWillUnmount: function() {
        this.serverRequest.abort();
    },
    getData: function(url) {
      var _this = this
      this.serverRequest = $.getJSON(url)
          .done(function(result, status) {
              console.log("incident done: ", status)
          })
          .fail(function(jqXHR, status, error) {
              console.log("incident fail: ", status);
              alert(status + ": Failed to retrieve the incident");
          })
          .success(function(result, status) {
              console.log("Retrieved incident")
              _this.setState({
                  incident: result
              });
          })
          .error(function(result, status) {
              console.log("error: ", status);
          })
          // .bind(this);
    },
        render: function() {
          var threads = this.state.incident.incidentthreads_set.map( function(thread, index) {
              var body = thread.text;
              var preformat = false;
              if (thread.contenttype == "text/plain") {
                preformat = true;
                body = "<pre>" + body + "</pre>";
              }
              var header = thread.eventtype ? thread.eventtype : ""
              var row =
              <Panel header={header  + ' ' + thread.channel} bsStyle="success">
                <Grid fluid={true}>
                  <Row>
                  <Col md={4} mdPush={8}>{ thread.contact ? thread.contact : thread.account }
                   { thread.createdtime }</Col>
                </Row>
                <Row>
                  <Col md={12}><div dangerouslySetInnerHTML={{__html:  body }}/></Col>
                </Row>
                </Grid>
                </Panel>
              return row;
          });

            return (
              <div>
                <Grid fluid={true}>
                <form className="form-horizontal">
                    <Row className="show-grid">
                    <Col md={6}>
                      <FormControls.Static label="Subject"
                        bsSize="small"
                        labelClassName="col-xs-2"
                        wrapperClassName="col-xs-10"
                        value={this.state.incident.subject}
                      />
                      <FormControls.Static label="Incident Type"
                        bsSize="small"
                        labelClassName="col-xs-2"
                        wrapperClassName="col-xs-10"
                        value={this.state.incident.incidenttype}
                      />
                      <FormControls.Static label="Category"
                        bsSize="small"
                        labelClassName="col-xs-2"
                        wrapperClassName="col-xs-10"
                        value={this.state.incident.category}
                      />
                      <FormControls.Static label="Severity"
                        bsSize="small"
                        labelClassName="col-xs-2"
                        wrapperClassName="col-xs-10"
                        value={this.state.incident.severity}
                      />
                    </Col>
                    <Col md={6}>
                      <FormControls.Static label="Date Created"
                        bsSize="small" labelClassName="col-xs-2"
                        wrapperClassName="col-xs-10"
                        value={this.state.incident.createdtime}
                      />
                      <FormControls.Static label="Date Closed"
                        bsSize="small" labelClassName="col-xs-2"
                        wrapperClassName="col-xs-10"
                        value={this.state.incident.closedtime}
                      />
                      <FormControls.Static label="Organization"
                        bsSize="small" labelClassName="col-xs-2"
                        wrapperClassName="col-xs-10"
                        value={this.state.incident.organization}
                      />
                      <FormControls.Static label="Contact"
                        bsSize="small" labelClassName="col-xs-2"
                        wrapperClassName="col-xs-10"
                        value={this.state.incident.contact}
                      />
                    </Col>
                    </Row>
                </form>
                </Grid>
                <IncidentTabs
                  messages={threads}
                  attachments={this.state.incident.attachments}
                  />
                </div>
            );
        }
});

var IncidentList = React.createClass({
    getDefaultProps: function() {
        return {
                filter_field: null,
                filter_value: null
        };
    },
  getInitialState: function() {
          return {
                  incident_list: {'results':[]},
                  activePage: 1,
                  alertVisible: false,
                  detailsVisible: false,
                  selectedRow: {incidentid: 0, url: '', incidentthreads_set: []}
          };
  },
  componentDidMount: function() {
    this.getData(this.build_url());
  },

  componentWillUnmount: function() {
      this.serverRequest.abort();
  },

  getData: function(url) {
    var _this = this
    this.serverRequest = $.getJSON(url)
        .done(function(result, status) {
            console.log("done: ", status)
        })
        .fail(function(jqXHR, status, error) {
            console.log("fail: ", status);
              alert(status + ": Failed to get the incidents");
        })
        .success(function(result, status) {
            console.log("Retrieved incidents")
            _this.setState({
                incident_list: result
            });
        })
        .error(function(result, status) {
            console.log("error: ", status);
        })
        // .bind(this);
  },
  build_url() {
      var base_url = this.props.source;
      var page = this.state.activePage;
      var url = base_url + '?'  + 'page=' + page
      if (this.props.filter_field) {
          var filter = this.props.filter_field + '=' + this.props.filter_value;
          url = url + '&' + filter;
      };
      return url;
  },
  open(row, isSelected) {
    var url = row.url;
    this.setState({
        detailsVisible: true,
        selectedRow: row
    });
//    React.render(<OrgAppComponent source={url}/>, document.getElementById('tabbed'));
  },
  close_details() {
      this.setState({
          detailsVisible: false
      });
  },
  selectPage(event, selectedEvent) {
    this.setState({
        activePage: selectedEvent.eventKey
    });
    this.getData(this.build_url());
  },
  search() {
    this.getData(this.build_url() + '&search=' + this.refs.input.getValue());
  },
  render: function() {
    let selectRowProp = {
        mode: "radio",
        clickToSelect: true,
        // bgColor: "rgb(0, 193, 0)",
        onSelect: this.open
    };
    let last_page = Math.ceil(this.state.incident_list.count / 20)

    if (this.state.detailsVisible) {
        return (
            <Panel header={this.state.selectedRow.referencenumber} bsStyle="primary">
            <Button
                bsStyle="primary"
                disabled={false}
                onClick={this.close_details}
                >
                Close
          </Button>
          <IncidentDetails incident={this.state.selectedRow}/>
          </Panel>
        );
    } else {
    return (
        <div id="incidentlist">
      <Panel>
      <form className="form-horizontal" action="#" onsubmit="{this.search}">
        <Input type="text"
          placeholder="Search"
          ref="input"
          groupClassName="standalone"
          onChange={this.search}
          hasFeedback
          wrapperClassName="col-xs-4"
        />
      </form>
      <BootstrapTable data={this.state.incident_list.results} paginationSize={5}
        search={false}
        paginationSize={5}
        pagination={false}
        selectRow={selectRowProp}
        striped={true}
        hover={true}
        //height="400"
      >
            <TableHeaderColumn dataField="incidentid" isKey={true} hidden={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
            <TableHeaderColumn dataField="referencenumber" width={200} dataSort={true}>Reference #</TableHeaderColumn>
            <TableHeaderColumn dataField="subject" >Subject</TableHeaderColumn>
      </BootstrapTable>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          items={last_page}
          maxButtons={5}
          activePage={this.state.activePage}
          onSelect={this.selectPage} />

        </Panel>
        </div>
      );
  };
  }

});

var ContactList = React.createClass({
    getInitialState() {
      return { showModal: false,
        selectedRow: {
          lookupname: '',
          contactid: 0,
          notes: []
        }
      };
    },

    close() {
      this.setState({ showModal: false });
    },

    open(row, isSelected) {
        var _this = this
        this.setState({ showModal: true, selectedRow: row });
        var url = row.url;
        if (true) {
            $.getJSON(url)
                .done(function(result, status) {
                        console.log("done: ", status)
            })
            .fail(function(jqXHR, status, error) {
                    console.log("fail: ", status)
                    alert(status + ": Failed to retrieve the contact");
                    _this.setState(_this.getInitialState());
            })
            .success(function(result, status) {
                console.log("getJSON success")
                _this.setState({
                    selectedRow: result
                });
            })
            .error(function(result, status) {
                console.log("error: ", status);
            })
        };
    },

    render() {
        let selectRowProp = {
            mode: "radio",
            clickToSelect: true,
            // bgColor: "rgb(0, 193, 0)",
            onSelect: this.open
        };

        return (
            <div>
            <BootstrapTable data={this.props.contacts} paginationSize={5} pagination={false} selectRow={selectRowProp} striped={true} hover={true}>
                  <TableHeaderColumn dataField="contactid" isKey={true} hidden={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
                  <TableHeaderColumn dataField="lookupname" dataSort={true}>Full Name</TableHeaderColumn>
                  <TableHeaderColumn dataField="email" dataSort={true} >Email Address</TableHeaderColumn>
                  <TableHeaderColumn dataField="phonenumber" >Office Phone</TableHeaderColumn>
              </BootstrapTable>
              <div className="static-modal">
                  <Modal show={this.state.showModal} onHide={this.close} className="modal modal-wide fade">
                    <Modal.Header closeButton>
                      <Modal.Title>{this.state.selectedRow.lookupname}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                    <Panel>
                    <Grid>
                    <form className="form-horizontal">
                        <Row className="show-grid">
                        <Col md={6} mdPush={6}>
                        <FormControls.Static label="Phone" bsSize="small" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.state.selectedRow.phonenumber} />
                        <FormControls.Static label="Mobile Phone" bsSize="small" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.state.selectedRow.mobilephonenumber} />
                        <FormControls.Static label="Organization Name" bsSize="small" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.state.selectedRow.organization} />
                        <FormControls.Static label="Division" bsSize="small" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.state.selectedRow.division}></FormControls.Static>
                    </Col>
                    <Col md={6} mdPull={6}>
                    <FormControls.Static label="Email" bsSize="small" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.state.selectedRow.email} />
                    <FormControls.Static label="First Name" bsSize="small" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.state.selectedRow.firstname} />
                    <FormControls.Static label="Last Name" bsSize="small" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={this.state.selectedRow.lastname} />
                    </Col></Row>
                    </form>
                    </Grid>
                    <ContactTabs contact={this.state.selectedRow}/>
                    </Panel>
                    </Modal.Body>

                    <Modal.Footer>
                      <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>

                  </Modal>
                </div>
            </div>
        );
    }
});
var OrgAppComponent = React.createClass({
    getInitialState: function() {
            return {
                    organization: {
                      organizationid: this.props.organizationid,
                      contacts: [],
                      notes: [],
                      incidents: []
                    },
            };
    },
    componentDidMount: function() {
        var _this = this
        this.serverRequest = $.getJSON(this.props.source)
            .done(function(result, status) {
                console.log("done: ", status)
            })
            .fail(function(jqXHR, status, error) {
                console.log("fail: ", status);
                alert(status + ": Failed to retrieve the organization");
                _this.setState(_this.getInitialState());
            })
            .success(function(result, status) {
                console.log("getJSON success")
                var organization=result;
                _this.setState({
                    organization: organization
                });
            })
            .error(function(result, status) {
                console.log("error: ", status);
            })
            //.bind(_this);
    },

    componentWillUnmount: function() {
        this.serverRequest.abort();
    },

    render: function() {
        return (
            <div>
                <OrgComponent organization={this.state.organization}/>
                <OrgTabs organization={this.state.organization}/>
            </div>
        );
    }
});

var OrgSearchComponent = React.createClass({
  getInitialState: function() {
          return {
                  organization_list: {'results':[]},
                  activePage: 1,
                  alertVisible: false
          };
  },
  getData: function(url) {
    var _this = this
    this.serverRequest = $.getJSON(url)
        .done(function(result, status) {
            console.log("done: ", status)
        })
        .fail(function(jqXHR, status, error) {
            console.log("fail: ", status);
            alert(status + ": Failed to pull the list of organizations");
        })
        .success(function(result, status) {
            console.log("Retrieved orgs")
            _this.setState({
                organization_list: result
            });
        })
        .error(function(result, status) {
            console.log("error: ", status);
        })
        // .bind(this);
  },
  componentDidMount: function() {
    this.getData(this.props.source);
  },

  componentWillUnmount: function() {
      this.serverRequest.abort();
  },
  open(row, isSelected) {
    var url = row.url;
    React.render(<OrgAppComponent source={url} organizationid={row.organizationid}/>, document.getElementById('tabbed'));
  },
  selectPage(event, selectedEvent) {
    this.setState({
        activePage: selectedEvent.eventKey
    });
    this.getData(this.props.source + '?page=' + selectedEvent.eventKey);
  },
  search() {
    this.getData(this.props.source + '?search=' + this.refs.input.getValue());
  },
  render: function() {
    let selectRowProp = {
        mode: "radio",
        clickToSelect: true,
        // bgColor: "rgb(0, 193, 0)",
        onSelect: this.open
    };
    let last_page = Math.ceil(this.state.organization_list.count / 20)

    return (
      <Panel>
      <form className="form-horizontal" action="#" onsubmit="{this.search}">
        <Input type="text"
          placeholder="Search"
          ref="input"
          groupClassName="standalone"
          onChange={this.search}
          wrapperClassName="col-xs-4"
        />
      </form>
      <BootstrapTable data={this.state.organization_list.results}
        search={false}
        paginationSize={5}
        pagination={false}
        selectRow={selectRowProp}
        striped={true}
        hover={true}
        //height="400"
      >
            <TableHeaderColumn dataField="organizationid" hidden={true} isKey={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
            <TableHeaderColumn dataField="lookupname" dataSort={true}>Organization</TableHeaderColumn>
        </BootstrapTable>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          items={last_page}
          maxButtons={5}
          activePage={this.state.activePage}
          onSelect={this.selectPage} />
        </Panel>
      );
  }
});
React.render(<SiteHeader
                    headerText="Test App"
                    tagline="Read-Only RightNowWeb Data"
                    preferImageToH1="true"
                    navItems={  ['Organizations', 'Incidents'] }
                    imgSrc="https://www.anpi.com/images/anpi_logo.png"
                    imgAlt="ANPI logo" />,
                    document.getElementById('content'));
// React.render(<OrgAppComponent source="/rnw/api/organization/5131"/>, document.getElementById('tabbed'));
React.render(<OrgSearchComponent source="/rnw/api/organizations"/>, document.getElementById('tabbed'));
