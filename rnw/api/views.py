from .. import models
import django_filters
from rest_framework import viewsets, filters, response
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer, BrowsableAPIRenderer
from rest_framework.decorators import detail_route
from .serializers import IncidentSerializer, IncidentListSerializer, IncidentThreadsSerializer
from .serializers import OrganizationSerializer, OrganizationListSerializer
from .serializers import ContactSerializer, ContactListSerializer

class IncidentFilter(filters.FilterSet):
    contactid = django_filters.NumberFilter(name="incidentthreads__contactid")

    class Meta:
        model = models.Incident
        fields = ['contactid', 'organizationid']

class IncidentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Incident.objects.all()
    #serializer_class = IncidentSerializer
    #template_name = 'rnw/incident_list.html'
    renderer_classes = (TemplateHTMLRenderer, JSONRenderer, BrowsableAPIRenderer)
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend)
    search_fields = ('subject', 'referencenumber')
    filter_class = IncidentFilter

    def get_serializer_class(self):
        serializer = IncidentSerializer
        if self.action == 'list':
            serializer = IncidentListSerializer
        return serializer

    def get_template_names(self):
        template = 'base.html'
        if self.action == 'list':
            template = 'rnw/incident_list.html'
        elif self.action == 'retrieve':
            template = 'rnw/incident_detail.html'
        return [template]



class OrganizationViewSet(viewsets.ReadOnlyModelViewSet):
    """
    .. http:get:: /rnw/api/organizations/(int:organizationid)

   Retrieves a list of organizations or an individual organization if the ID
   is provided.

   **Example request**:

   .. sourcecode:: http

      GET /rnw/api/organizations/11142/ HTTP/1.1
      Host: example.com
      Accept: application/json, text/javascript

    """
    queryset = models.Organization.objects.all()
    renderer_classes = (TemplateHTMLRenderer, JSONRenderer, BrowsableAPIRenderer)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('lookupname', )

    def get_serializer_class(self):
        serializer = OrganizationSerializer
        if self.action == 'list':
            serializer = OrganizationListSerializer
        return serializer

    def get_template_names(self):
        template = 'base.html'
        if self.action == 'list':
            template = 'rnw/organization_list.html'
        elif self.action == 'retrieve':
            template = 'rnw/organization_detail.html'
        return [template]


class ContactViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Contact.objects.all()

    def get_serializer_class(self):
        serializer = ContactSerializer
        if self.action == 'list':
            serializer = ContactListSerializer
        return serializer
