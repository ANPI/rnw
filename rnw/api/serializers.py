from rest_framework import serializers
from rest_framework.reverse import reverse, reverse_lazy
from .. import models

class AttachmentSerializer(serializers.ModelSerializer):
    filestatus = serializers.ReadOnlyField(source='filestatusid.filestatus', read_only=True)
    url = serializers.ReadOnlyField(source='attachment_url', read_only=True)
    class Meta:
        model = models.Attachment

class ContactListSerializer(serializers.ModelSerializer):
    url = serializers.ReadOnlyField(source='get_api_url', read_only=True)
    class Meta:
        model = models.Contact
        fields = ('contactid', 'lookupname', 'phonenumber', 'url' )

class IncidentThreadsSerializer(serializers.ModelSerializer):
    channel = serializers.ReadOnlyField(source='channelid.channel', read_only=True)
    contact = serializers.ReadOnlyField(source='contactid.lookupname', read_only=True)
    entrytype = serializers.ReadOnlyField(source='entrytypeid.entrytype', read_only=True)
    contenttype = serializers.ReadOnlyField(source='contenttypeid.contenttype', read_only=True)
    account = serializers.ReadOnlyField(source='accountid.displayname', read_only=True)

    class Meta:
        model = models.IncidentThreads

class IncidentSerializer(serializers.ModelSerializer):
    incidentthreads_set = IncidentThreadsSerializer(many=True, read_only=True)
    organization = serializers.ReadOnlyField(source='organizationid.lookupname',
                                          read_only=True)
    incidenttype = serializers.ReadOnlyField(source='issueid.issue', read_only=True)
    category = serializers.ReadOnlyField(source='categoryid.servicecategory', read_only=True)
    severity = serializers.ReadOnlyField(source='severityid.severity', read_only=True)
    contact = serializers.ReadOnlyField(source='contactid.lookupname', read_only=True)
    attachments = AttachmentSerializer(source='attachment_set',
        many=True,
        read_only=True
    )
    class Meta:
        model = models.Incident


class IncidentListSerializer(IncidentSerializer):
    url = serializers.ReadOnlyField(source='get_api_url', read_only=True)
    class Meta:
        model = models.Incident
        fields = ('incidentid', 'subject', 'createdtime',
            'closedtime', 'referencenumber', 'lastresponsetime', 'url' )

class NotesListSerializer(serializers.ModelSerializer):
    channel = serializers.ReadOnlyField(source='channelid.channel', read_only=True)
    class Meta:
        model = models.Note


class ContactSerializer(serializers.ModelSerializer):
    organization = serializers.ReadOnlyField(source='organizationid.lookupname', read_only=True)
    division = serializers.ReadOnlyField(source='organizationid.divisionid.division', read_only=True)
    attachments = AttachmentSerializer(source='attachment_set',
        many=True,
        read_only=True
    )

    notes = NotesListSerializer(source='note_set',
    many=True,
    read_only=True
    )
    class Meta:
        model = models.Contact
        fields = ('contactid', 'lookupname', 'firstname', 'lastname',
            'phonenumber', 'mobilenumber', 'title', 'organization', 'division',
            'notes', 'attachments'
            )

class OrganizationListSerializer(serializers.ModelSerializer):
    url = serializers.ReadOnlyField(source='get_api_url', read_only=True)
    class Meta:
        model = models.Organization
        fields = ('organizationid', 'lookupname', 'url')

class OrganizationSerializer(serializers.ModelSerializer):
    division = serializers.ReadOnlyField(source='divisionid.division', read_only=True)

    attachments = AttachmentSerializer(source='attachment_set',
        many=True,
        read_only=True
    )
    contacts = ContactListSerializer(source='contact_set',
    many=True,
    read_only=True
    )
    incidents = IncidentListSerializer(source='incident_set',
    many=True,
    read_only=True
    )
    notes = NotesListSerializer(source='note_set',
    many=True,
    read_only=True
    )

    class Meta:
        model = models.Organization
        fields = ('organizationid', 'division',
            'lookupname', 'contacts', 'incidents',
            'notes', 'attachments' )
